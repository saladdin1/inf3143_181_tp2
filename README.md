# INF3143 - TP2

Afin de vérifier votre travail, le TP2 vient avec une série de tests automatisés à lancer vous-même.

Les tests ne fonctionnent que sur un système Unix (Linux / MacOS). Si vous travaillez sur Windows, il vous faudra utiliser les serveurs `java` ou `malt` de labunix.

## Lancer les tests

Il y'a 3 séries de tests:
* Le test du main `make check-main`
* Le lancement des tests unitaires `make check-tests`
* Les tests des contrats `make check-contrats`

Vous pouvez utiliser la commande `make check` pour lancer tous les tests en une commande.

## Exécuter un test de contrat

Le script `./run_contrat.sh` permet de compiler et lancer un fichier avec Cofoja.

Par exemple pour exécuter le fichier de test du contrat `TestShip01.java`:

~~~sh
./run_contrat.sh TestShip01
~~~

Cette commande va compiler puis exécuter le code avec Cofoja.

## Utilisation depuis Java/Malt

Voici les étapes à suivre pour travailler avec les serveurs Java ou Malt:

1. Connectez-vous au serveur:

	Depuis Linux / MacOS:
	~~~sh
	ssh CODEMS@java.labunix.uqam.ca
	~~~

	Depuis Windows: utiliser Putty.

2. Clonez le dépôt du TP:

	~~~sh
	git clone https://gitlab.com/Morriar/inf3143_181_tp2.git
	cd inf3143_173_tp2
	~~~

3. Modifiez les fichiers afin de rajouter le Main, les tests unitaires et les contrats.

4. Lancer les tests:

	~~~sh
	make check
	~~~
