/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

import java.util.List;

/**
 * The game is composed of turns.
 *
 * For each turn there is two rounds.
 */
public class Turn {

    /**
     * Turn number for display.
     */
    Integer turnNumber;

    /**
     * Players participating in this turn.
     */
    List<Player> players;

    /**
     * The two rounds to play.
     */
    List<Round> rounds;

    public Turn(Integer turnNumber, List<Player> players, List<Round> rounds) {
        this.turnNumber = turnNumber;
        this.players = players;
        this.rounds = rounds;
        System.out.println("Turn #" + turnNumber);
    }

    /**
     * Play all the rounds in this turn and check if the game is over.
     *
     * @return true if the game is finished.
     */
    public Boolean playTurn() {
        for (Round round : rounds) {
            round.playRound();
            if (round.target.ship.isDestroyed()) {
                endTurn();
                System.out.println("Game over: " + round.attackee + " wins!");
                return true;
            }
        }
        for (Player player : players) {
            player.ship.reload();
        }
        endTurn();
        return false;
    }

    /**
     * Display the end turn stats.
     */
    private void endTurn() {
        System.out.println("End turn #" + turnNumber);
        for (Player player : players) {
            System.out.println(" * " + player.ship + " (" + player.ship.status() + ")");
        }
        System.out.print("\n");
    }

}
