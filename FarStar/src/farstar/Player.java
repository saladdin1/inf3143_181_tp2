/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

import java.util.ArrayList;
import java.util.List;

/**
 * A Player can by a space ship and upgrade it to fight other players.
 */
public class Player {

    /**
     * Player's name
     */
    protected String name;

    /**
     * Amount of credits the player can use to by ships and upgrades.
     */
    protected Integer credits;

    /**
     * Current ship owned by the player.
     */
    protected Ship ship;

    /**
     * All the ships and upgrades owned by the player.
     */
    protected List<Item> ownedItems = new ArrayList<>();

    public Player(String name, Integer credits) {
        this.name = name;
        this.credits = credits;
    }

    /**
     * Buy an item.
     *
     * @param item the item to by
     * @throws NotEnoughCreditsException if not enough credits
     *
     * Once bought the item can be found in `Player::ownedItems`.
     */
    public void buy(Item item) throws NotEnoughCreditsException {
        if (item.getCreditsCost() > credits) {
            throw new NotEnoughCreditsException();
        }
        credits -= item.getCreditsCost();
        ownedItems.add(item);
    }
    public void setShip(Ship ship) {
        ship.setOwner(this);
        this.ship = ship;
    }

    public Ship getShip() {
        return ship;
    }

    public String getName() {
        return name;
    }

    public Integer getCredits() {
        return credits;
    }
    
    public List<Item> getOwnedItems() {
        return ownedItems;
    }
    
    @Override
    public String toString() {
        return name;
    }

    public String status() {
        return "credits: " + credits;
    }
}
